const http=require('http');

//create a variable 'port' to store the port number

const port = 3000

//create a variable 'server' that stores the output of the 'createSever' Method

const server = http.createServer((request,response) => {

		//localhost:4000/greeting
		//Access the /greeting route and will return a message of "Hello World"
		//'request' is an object that is sent via the client (browser)
		//'url' property refers to the url or the link in the browser

		// if (request.url == '/greeting'){
		// 	response.writeHead(200, {'Content-Type': 'text/plain'})
		// 	response.end('hello world')
		// }

		if (request.url == '/login'){
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Welcome to the login page.')
		}else if(request.url !== '/login'){
			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Im sorry the page you are looking for cannot be found.')
		}

		//Acess the /homepage route and will return a message of " This is the homepage"
});

//use the 'server' and 'port' variables created above
server.listen(port);

//when server is running, console will print the message:
console.log(`Server now accessible at localhost: ${port}`)	